
# ***********************
# ***********************
# Makefile
# ***********************
# ***********************


# ***********************
# Tools configuration 
# ***********************


# ***********************
# Makefile targets
# ***********************
help: ## Print this help.	
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

version: ## Obtain current version number
	@echo 1.0.0

clean_project: ## Clean project 
	@echo "[$@] Clean project downloaded dependencies and generated binaries"
	@rm -rf binaries
	@rm -rf target
	
build: clean_project ## Build the project
	@echo "[$@] Install dependencies the project "
	@mkdir binaries
	@mkdir target
	@touch binaries/bin1
	@touch binaries/bin2
	@echo "This is the generated artifact after compiling the code" > target/artifact.txt
	
unit_test: ## Run unit tests
	@echo "[$@] Running Unit tests"
	@ls binaries/

integration_test: ## Run integration tests
	@echo "[$@] Running Integration tests"
	@ls binaries/

e2e_test: ## Run e2e tests
	@echo "[$@] Running E2E tests"
	@ls binaries/

qa: ## Run quality analysis
	@echo "[$@] Running SonarQube analysis"

container_scan: ## Run Docker image static analysis
	@echo "[$@] Running Anchore analysis"

sca: ## Run Software Composition analysis
	@echo "[$@] Nothing to do here."

archive: ## Archive artefacts and Docker images
	@echo "[$@] Archiving artefacts"
	@echo "[$@] Archiving docker image"

deploy: ## Archive artefacts and Docker images
	@echo "[$@] Deploying to environment"
	@cat target/artifact.txt

health_check:
	@echo "[$@] Checking environment in url: $(ENVIRONMENT_URL)"
